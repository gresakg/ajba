<?php 

class Ajba{

	/**
	 * Theme path
	 * @var string
	 */
	protected $path;

	/**
	 * Wordpress and theme combined version
	 * @var string
	 */
	protected $version;

	public $crumbs = array();

	protected static $instance;

	private function __construct($path){
		$this->path = $path;
		$this->set_version();
		$this->set_filters();
	} 

	public function set_filters() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'custom-logo' );
		add_action("wp_enqueue_scripts",array($this,"enqueue_scripts"));
		add_action('init', array($this, "disable_wp_emojicons"));
		add_action('init', array($this,"post_type_support"));
		add_action('init', array($this,"register_menus"));
		add_action('init', array($this,"sidebars"));
		add_action('after_body_tag', array($this,'breadcrumbs'),10,2);

		add_action('customize_register', "GGCustomizer::instance",10,1);
	}

	public function post_type_support() {
		add_post_type_support( 'post', 'thumbnail' );
	}

	public function enqueue_scripts() {
		wp_enqueue_style( "base", get_stylesheet_directory_uri()."/css/index.css");//,array(),$this->version 
		wp_enqueue_script( "main", get_template_directory_uri()."/js/all.min.js", array("jquery"), NULL, true );
		add_action( 'init', array($this, 'register_menus') );
	}

	public function register_menus() {
		register_nav_menus( array(
				'main-menu' => __('Main menu')
			) );
	}

	public function sidebars() {
		register_sidebar( array(
			'name' => 'Footer',
			'id' => 'footer',
			'before_widget' => '<div id="%1$s" class="widget col %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="widgettitle">',
			'after_title'   => '</h2>',
		) );
	}

	public function breadcrumbs() {
		$categories = get_the_category();
		$post = get_post();
		if(empty($categories) || empty($post)) return;
		$alen = 0;
		foreach($categories as $category) {
			$tmp = get_ancestors($category->term_id,"category","taxonomy");
			if(count($tmp) >= $alen) {
				$ancestors = $tmp;
				$alen = count($tmp);
				$cat = $category;
			}
		}
		foreach($ancestors as $ancestor) {
			$this->set_crumb_item($ancestor);
		}
		$this->set_crumb_item($cat);
		$this->set_crumb_item($post);

		get_template_part('parts/breadcrumbs');

	}

	protected function set_crumb_item($arg) {
		if(is_numeric($arg)) {
			$cat = get_term_by('id',$arg,'category');
			$item['name'] = $cat->name;
			$item['url'] = get_term_link($arg,'category');
		}
		if(is_a($arg,"WP_Term")) {
			$item['name'] = $arg->name;
			$item['url'] = get_term_link($arg);
		}
		if(is_a($arg,"WP_Post")) {
			$item['name'] = $arg->post_title;
			$item['url'] = get_permalink($arg);
		}

		$this->crumbs[] = $item;
	}

	public function disable_wp_emojicons() {
	  // all actions related to emojis
	  remove_action( 'admin_print_styles', 'print_emoji_styles' );
	  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	  remove_action( 'wp_print_styles', 'print_emoji_styles' );
	  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	}

	protected function set_version() {
		$theme = wp_get_theme();
		$wp_version = get_bloginfo( 'version' );
		$this->version =  $wp_version ."." . $theme->get('Version');
	}

	public static function instance($path) {

        if (self::$instance === null) {
            self::$instance = new self($path);
        }
        return self::$instance;
    }
}
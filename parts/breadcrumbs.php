<?php 

$ajba = Ajba::instance(false); 

//var_dump($ajba->crumbs);

?>
<div class="breadcrumbs container">
	<span><a href="<?php echo get_home_url(); ?>"><?php echo __('Home','ajba'); ?></a></span>
	<?php foreach($ajba->crumbs as $crumb): ?>
	<span>&raquo; <a href="<?php echo $crumb['url']; ?>"><?php echo $crumb['name']; ?></a></span>
	<?php endforeach; ?>
</div>
<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "BreadcrumbList",
	  "itemListElement": [{
	    "@type": "ListItem",
	    "position": 1,
	    "item": {
	      "@id": "<?php echo get_home_url(); ?>",
	      "name": "<?php echo __('Home', 'over'); ?>"
	    }
	  }
	<?php foreach($ajba->crumbs as $crumb): ?>
	 ,{
	    "@type": "ListItem",
	    "position": 2,
	    "item": {
		      "@id": "<?php echo $crumb['url']; ?>",
		      "name": "<?php echo $crumb['name']; ?>"
		    }
		}
	<?php endforeach; ?> 
	]
	}
</script>
